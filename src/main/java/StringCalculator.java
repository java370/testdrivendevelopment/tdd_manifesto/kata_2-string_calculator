import java.util.IllegalFormatCodePointException;
import java.util.IllegalFormatException;
//https://vimeo.com/8506325
//https://github.com/pjmolina/String-calculator-kata-in-java/blob/p5/src/main/java/metadev/kata/stringcalc/StringCalculator.java
//https://github.com/pjmolina/String-calculator-kata-in-java/blob/p5/src/test/java/metadev/kata/stringcalc/StringCalculatorTest.java


public class StringCalculator {
    public static int Add(String s) {
        if (s.isEmpty()) return 0;
        else if (s.charAt(s.length() - 1) == ',') {
            throw new IllegalArgumentException("Invalid Input!");
        } else if (s.startsWith("//")) {
            int n_line_position = s.indexOf('\n');
            String delimiter = "";
            String valueString = "";
            if (n_line_position > 3) {
                delimiter = s.substring(2, n_line_position);
            } else {
                delimiter = "\\" + String.valueOf(s.charAt(2));
                valueString = s.substring(n_line_position + 1);
            }
            valueString = s.substring(n_line_position + 1);

            String[] tmpTokens = valueString.split(delimiter);
            int sum = 0;
            for (int i = 0; i < tmpTokens.length; i++) {
                System.out.print(tmpTokens[i] + " -- ");
                sum += toInt(tmpTokens[i]);
            }
            return sum;
        } else if (s.contains(",") || s.contains("\n")) {
            String[] tokens = s.split("[, |\n]+");
            int sum = 0;
            for (int i = 0; i < tokens.length; i++) {
                sum += toInt(tokens[i]);
            }
            return sum;
        } else return Integer.parseInt(s);
    }

    private static int toInt(String s) throws NumberFormatException {
        return Integer.parseInt(s);
    }
}
