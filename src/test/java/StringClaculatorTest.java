import org.junit.jupiter.api.Test;

import java.util.IllegalFormatException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringClaculatorTest {
    @Test
    void shouldReturnZeroOnEmptyString(){
        assertEquals(0,StringCalculator.Add(""));
    }
    @Test
    void shouldReturnAnIntegerRespectToThatInteger(){
        assertEquals(2,StringCalculator.Add("2"));
    }
    @Test
    void shouldReturnIntegerValueOfTwoNumbersSeparatedByComma(){
        assertEquals(7,StringCalculator.Add("2,5"));
    }
    @Test
    void shouldReturnSumOfMultipleNumbers(){
        assertEquals(10,StringCalculator.Add("1,2,3,4"));
        assertEquals(0,StringCalculator.Add("-1,0,1,0"));
    }
    @Test
    void shouldAcceptNewLineAsValidDelimeter(){
        assertEquals(9,StringCalculator.Add("3,4\n2"));
        assertEquals(9,StringCalculator.Add("3\n4\n2"));
        assertEquals(9,StringCalculator.Add("3\n4,2"));
    }
    @Test
    void ShouldThrowAnExceptionIfThereIsASeparatorAtTheEnd(){
        var exception = assertThrows(IllegalArgumentException.class, ()->StringCalculator.Add("4,5,"));
        assertEquals("Invalid Input!",exception.getMessage());
    }
    @Test
    void shouldSupportDifferentDelimeters(){
        assertEquals(4,StringCalculator.Add("//;\n1;3"));
        assertEquals(4,StringCalculator.Add("//|\n1|3"));
        assertEquals(4,StringCalculator.Add("//$\n1$3"));
        assertEquals(4,StringCalculator.Add("//%\n1%3"));
        assertEquals(4,StringCalculator.Add("//^\n1^3"));
        assertEquals(4,StringCalculator.Add("//sep\n1sep3"));
        assertEquals(4,StringCalculator.Add("//&oo\n1&oo3"));
//        assertEquals(4,StringCalculator.Add("//*^@\n1*^@3"));

    }

}
