import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Kata2Test {
    private Kata2 mytest;
    @BeforeEach
    void setUp(){
        mytest = new Kata2();
    }

//1. The method can take up to two numbers, separated by commas, and will return their sum as a result. So the inputs can be: “”, “1”, “1,2”. For an empty string, it will return 0.
    @Test
    public void GivenEmptyString_ShouldReturnZero(){
        assertEquals(0,mytest.Add(""));
    }
    @Test
    public void GivenString_ShouldReturnSameInteger(){
        assertEquals(3,mytest.Add("3"));
    }
    @Test
    public void GivenString0fTwoValues_ShouldReturnSumInIntegerValue(){
        assertEquals(20,mytest.Add("11,9"));
    }

//2. Allow the add method to handle an unknown number of arguments
    @Test
    void GivenUnknownNumberOfInputShouldGetZero(){
        assertEquals(0,mytest.Add("4,5,6,7,8,9"));
    }
    @Test
    void ShouldReturnOutputForMultipleInputs(){
        assertEquals(45, mytest.Add("1,2,3,4,5,6,7,8,9"));
        assertEquals(50, mytest.Add("1,2,3,4,5,6,7,8,9,5"));
    }
    @Test
    void ShouldThrowAnExceptionIfThereIsCommaAtTheEnd(){
        var exception = assertThrows(IllegalArgumentException.class,()->mytest.Add("4,5,"));
        assertEquals("Invalid Argument at the End!",exception.getMessage());
    }

    @Test
    void ShouldHandleDelimeteres(){
        assertEquals(4,mytest.Add("//;\n1;3"));
    }

    @Test
    void shouldHandleAllKindOfDelimeters(){
//        assertEquals(7,mytest.Add("//sep\n2sep5"));
        assertEquals(6,mytest.Add("//|\n1|2|3"));
//        assertNotEquals(9,mytest.Add("//.\n2.5"));
//        assertNotEquals(9,mytest.Add("//$\n2$5"));
    }
}
